## GoLang Examples

API Service for Books (CRUD) Default port is 8080. `http://localhost:8080`

- Database: [sqlite3](https://github.com/mattn/go-sqlite3)
- HTTP Server: [fasthttp](https://github.com/valyala/fasthttp)
- HTTP Route: [fasthttprouter](https://github.com/buaazp/fasthttprouter)
- ENV: [godotenv](github.com/joho/godotenv)

#### How to run (note: run make once)
```
make
go run .
```

#### How to run tests
```
go test ./tests/...
```

### How to run test and generate coverage report
```
go test -v -failfast -cover -coverpkg ./... ./tests/... -coverprofile=tests/report/cover.out | tee tests/report/test.out && go tool cover -html=tests/report/cover.out -o tests/report/coverage.html
```

Coverage report will be at `/tests/report/coverage.html`

#### API definitions
1. `POST` => `/books`   

    Request body example:
   ```json
   {
     "name": "Cathedral",
     "author": "Raymond Carver",
     "description": "abc...."
   }
   ```
2. `GET` => `/books`
3. `GET` => `/books/:id`
4. `PUT` => `/books/:id`

    Request body example:
   ```json
   {
     "name": "Cathedral 2",
     "author": "Alexander Carter",
     "description": "abc updated..."
   }
   ```
5. `DELETE` => `/books/:id`
6. `GET` => `/metrics` (Prometheus metrics)

#### Use of Development Patterns & Techniques
1. Singleton (Context, Database pool)
2. Factory (Database operator)
3. Interface (Database connector, App bootstrapper)
4. Testing (SetupMainTest, SetupSubTest)
