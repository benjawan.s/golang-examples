package book

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/context"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/db/book"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/helper/http"
	httpModel "gitlab.com/nuttawutmalee/golang-examples/internal/pkg/model/http"
	"gitlab.com/nuttawutmalee/golang-examples/pkg/model/db"
)

func CreateBook(ctx *fasthttp.RequestCtx) {
	// get body
	body := ctx.Request.Body()

	var bookFromRequest db.Book

	err := json.Unmarshal(body, &bookFromRequest)
	if err != nil {
		errorMessage := "cannot unmarshal request body : " + err.Error()
		_ = http.JsonResponse(ctx, 500, httpModel.JsonResponse{
			Code:    500,
			Payload: nil,
			Message: &errorMessage,
		})

		return
	}

	// book operator
	bookOperator := book.New(context.Ctx().Pool)

	// use appCtx db pool to create a book
	err = bookOperator.Create(bookFromRequest)
	if err != nil {
		errorMessage := "cannot create a book : " + err.Error()
		_ = http.JsonResponse(ctx, 500, httpModel.JsonResponse{
			Code:    500,
			Payload: nil,
			Message: &errorMessage,
		})

		return
	}

	_ = http.JsonResponse(ctx, 200, httpModel.JsonResponse{
		Code:    201,
		Payload: nil,
	})
}
