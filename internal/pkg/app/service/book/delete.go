package book

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/context"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/db/book"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/helper/http"
	httpModel "gitlab.com/nuttawutmalee/golang-examples/internal/pkg/model/http"
	"strconv"
)

func DeleteBook(ctx *fasthttp.RequestCtx) {
	idString := ctx.UserValue("id").(string)

	id, err := strconv.Atoi(idString)
	if err != nil {
		errorMessage := "cannot convert id to integer : " + err.Error()
		_ = http.JsonResponse(ctx, 500, httpModel.JsonResponse{
			Code:    500,
			Payload: nil,
			Message: &errorMessage,
		})

		return
	}

	// book operator
	bookOperator := book.New(context.Ctx().Pool)

	// use appCtx db pool to delete a book
	err = bookOperator.Delete(id)
	if err != nil {
		errorMessage := "cannot delete a book : " + err.Error()
		_ = http.JsonResponse(ctx, 500, httpModel.JsonResponse{
			Code:    500,
			Payload: nil,
			Message: &errorMessage,
		})

		return
	}

	_ = http.JsonResponse(ctx, 200, httpModel.JsonResponse{
		Code:    204,
		Payload: nil,
	})
}
