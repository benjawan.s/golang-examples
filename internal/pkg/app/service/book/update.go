package book

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/context"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/db/book"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/helper/http"
	httpModel "gitlab.com/nuttawutmalee/golang-examples/internal/pkg/model/http"
	"gitlab.com/nuttawutmalee/golang-examples/pkg/model/db"
	"strconv"
)

func UpdateBook(ctx *fasthttp.RequestCtx) {
	idString := ctx.UserValue("id").(string)

	id, err := strconv.Atoi(idString)
	if err != nil {
		errorMessage := "cannot convert id to integer : " + err.Error()
		_ = http.JsonResponse(ctx, 500, httpModel.JsonResponse{
			Code:    500,
			Payload: nil,
			Message: &errorMessage,
		})

		return
	}

	// get body
	body := ctx.Request.Body()

	var bookFromRequest db.Book

	err = json.Unmarshal(body, &bookFromRequest)
	if err != nil {
		errorMessage := "cannot unmarshal request body : " + err.Error()
		_ = http.JsonResponse(ctx, 500, httpModel.JsonResponse{
			Code:    500,
			Payload: nil,
			Message: &errorMessage,
		})

		return
	}

	// book operator
	bookOperator := book.New(context.Ctx().Pool)

	// use appCtx db pool to update a book
	err = bookOperator.Update(id, bookFromRequest)
	if err != nil {
		errorMessage := "cannot update a book : " + err.Error()
		_ = http.JsonResponse(ctx, 500, httpModel.JsonResponse{
			Code:    500,
			Payload: nil,
			Message: &errorMessage,
		})

		return
	}

	// use appCtx db pool to get one book by id
	foundBook, err := bookOperator.GetOne(id)
	if err != nil {
		errorMessage := "cannot get a book : " + err.Error()
		_ = http.JsonResponse(ctx, 500, httpModel.JsonResponse{
			Code:    500,
			Payload: nil,
			Message: &errorMessage,
		})

		return
	}

	_ = http.JsonResponse(ctx, 200, httpModel.JsonResponse{
		Code:    200,
		Payload: foundBook,
	})

}
