package book

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/context"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/db/book"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/helper/http"
	httpModel "gitlab.com/nuttawutmalee/golang-examples/internal/pkg/model/http"
	"gitlab.com/nuttawutmalee/golang-examples/pkg/model/db"
)

func GetAllBooks(ctx *fasthttp.RequestCtx) {
	// book operator
	bookOperator := book.New(context.Ctx().Pool)

	// use appCtx db pool to get all books
	books, err := bookOperator.GetAll()
	if err != nil {
		errorMessage := "cannot get books : " + err.Error()
		_ = http.JsonResponse(ctx, 500, httpModel.JsonResponse{
			Code:    500,
			Payload: make([]db.Book, 0),
			Message: &errorMessage,
		})

		return
	}

	_ = http.JsonResponse(ctx, 200, httpModel.JsonResponse{
		Code:    200,
		Payload: books,
	})
}
