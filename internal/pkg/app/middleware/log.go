package middleware

import (
	"fmt"
	"github.com/valyala/fasthttp"
)

// Log is an example middleware that just prints out full url.
func Log(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		url := "n/a"
		body := ctx.Request.Body()
		method := ctx.Method()

		if ctx.Request.URI() != nil {
			url = ctx.Request.URI().String()
		}

		fmt.Printf("URL: %s\n", url)
		fmt.Printf("Method: %s\n", string(method))
		fmt.Printf("Body: %s\n", string(body))

		next(ctx)
	}
}
