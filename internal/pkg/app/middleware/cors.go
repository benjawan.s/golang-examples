package middleware

import "github.com/valyala/fasthttp"

// CORS is a middleware that allow cross origin response from a browser.
func CORS(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		// add these values in response header
		ctx.Response.Header.Set("Access-Control-Allow-Credentials", "true")
		ctx.Response.Header.Set("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization, X-Request-With, X-CLIENT-ID, X-CLIENT-SECRET")
		ctx.Response.Header.Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, DELETE, PUT")
		ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
		next(ctx)
	}
}
