package app

import (
	"github.com/buaazp/fasthttprouter"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/app/middleware"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/app/service/book"
)

// DefineRoutes is a root method that defines all necessary api services and attaches them with middleware.
func DefineRoutes(router *fasthttprouter.Router) {
	// metrics
	router.GET("/metrics", fasthttpadaptor.NewFastHTTPHandler(promhttp.Handler()))

	// books
	router.POST("/books", applyMiddleware(book.CreateBook))
	router.GET("/books", applyMiddleware(book.GetAllBooks))
	router.GET("/books/:id", applyMiddleware(book.GetOneBook))
	router.PUT("/books/:id", applyMiddleware(book.UpdateBook))
	router.DELETE("/books/:id", applyMiddleware(book.DeleteBook))
}

func applyMiddleware(handle fasthttp.RequestHandler) fasthttp.RequestHandler {
	// allows cors -> prints log -> api service
	return middleware.CORS(middleware.Log(handle))
}
