package app

import (
	"fmt"
	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/db"
	appInterface "gitlab.com/nuttawutmalee/golang-examples/pkg/interface/app"
	"os"
	"os/signal"
	"syscall"
)

type App struct{}

// New is a factory method that create a new object,
// in which this object has to be implemented according to Bootstrapper interface.
func New() appInterface.Bootstrapper {
	return &App{}
}

func (*App) Start() error {
	// connect to database
	err := db.Cluster().Connect()
	if err != nil {
		return err
	}

	fmt.Println("database is connected")

	// close database once hit CTRL+C or pod is removed
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-c

		_ = db.Cluster().Close()
		fmt.Println("database connection is closed")

		os.Exit(1)
	}()

	fmt.Println("application started")

	// define REST API routes
	router := fasthttprouter.New()
	DefineRoutes(router)

	return fasthttp.ListenAndServe(":8080", router.Handler)
}
