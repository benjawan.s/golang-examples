package db

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/context"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/helper/project_path"
	"gitlab.com/nuttawutmalee/golang-examples/pkg/interface/db"
	"os"
	"strings"
	"sync"
	"time"
)

type Pool struct {
	session *sql.DB
}

var pool *Pool
var once sync.Once

func Cluster() db.Pool {
	// create pool once,
	// so this pool can be anywhere
	once.Do(func() {
		pool = &Pool{}
	})
	return pool
}

func (p *Pool) Connect() error {
	if p.session != nil {
		// can use existing session pool
		return nil
	}

	dbFileLocation := os.Getenv("SQLITE_FILE_LOCATION")
	if dbFileLocation == "" {
		dbFileLocation = "sqlite.session"
	} else {
		dbFileLocation = strings.TrimSpace(dbFileLocation)
		dbFileLocation = strings.TrimLeft(dbFileLocation, ".")
		dbFileLocation = strings.TrimLeft(dbFileLocation, "/")
		dbFileLocation = strings.TrimLeft(dbFileLocation, "\\")
	}

	retries := 0

	for {
		database, err := sql.Open("sqlite3", project_path.Root+"/"+dbFileLocation)
		if err == nil {
			p.session = database

			// migrate tables
			err = CreateTables(p.session)
			if err != nil {
				return err
			}

			// create context object and store database in it, so we can use it later
			// Cluster() in this context is the one we connected to already
			context.Ctx().Pool = p.session

			return nil
		}

		if retries == 5 {
			return err
		}

		// retry connection every 5 seconds
		time.Sleep(5 * time.Second)
		retries++
	}
}

func (p *Pool) Session() (session interface{}, err error) {
	if p.session == nil {
		err := p.Connect()
		if err != nil {
			return nil, err
		}
	}

	return p.session, nil
}

func (p *Pool) Close() error {
	if p.session != nil {
		err := p.session.Close()
		if err != nil {
			return err
		}

		p.session = nil
	}

	return nil
}
