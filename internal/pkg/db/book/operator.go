package book

import (
	"database/sql"
	"errors"
	"gitlab.com/nuttawutmalee/golang-examples/pkg/interface/db/book"
	"gitlab.com/nuttawutmalee/golang-examples/pkg/model/db"
)

type Operator struct {
	pool *sql.DB
}

// This is a factory method that create a new object,
// in which this object has to be implemented according to book operator interface.
// In this case, we can develop with other database other than sql.
func New(pool *sql.DB) book.Operator {
	return &Operator{pool: pool}
}

func (o *Operator) Create(book db.Book) error {
	statementString := `INSERT INTO book (name, author, description) VALUES (?, ?, ?)`

	statement, err := o.pool.Prepare(statementString)
	if err != nil {
		return err
	}

	defer statement.Close()

	_, err = statement.Exec(book.Name, book.Author, book.Description)
	if err != nil {
		return err
	}

	return nil
}

func (o *Operator) GetAll() (books []db.Book, err error) {
	queryString := `SELECT id, name, author, description, createdAt, updatedAt FROM book`

	rows, err := o.pool.Query(queryString)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var emptyBook db.Book

		err := rows.Scan(
			&emptyBook.ID,
			&emptyBook.Name,
			&emptyBook.Author,
			&emptyBook.Description,
			&emptyBook.CreateAt,
			&emptyBook.UpdateAt)

		if err != nil {
			return nil, err
		}

		books = append(books, emptyBook)
	}

	if books == nil {
		books = make([]db.Book, 0)
	}

	return books, nil
}

func (o *Operator) GetOne(id int) (book *db.Book, err error) {
	queryString := `SELECT * FROM book WHERE id = ?`

	rows, err := o.pool.Query(queryString, id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var emptyBook db.Book

		err := rows.Scan(
			&emptyBook.ID,
			&emptyBook.Name,
			&emptyBook.Author,
			&emptyBook.Description,
			&emptyBook.CreateAt,
			&emptyBook.UpdateAt)

		if err != nil {
			return nil, err
		}

		book = &emptyBook
		break
	}

	if book == nil {
		return nil, errors.New("book not found")
	}

	return book, nil
}

func (o *Operator) Update(id int, book db.Book) error {
	statementString := `UPDATE book SET name=?, author=?, description=?, updatedAt=strftime('%Y-%m-%d %H:%M:%f', 'now', 'localtime') WHERE id=?`

	statement, err := o.pool.Prepare(statementString)
	if err != nil {
		return err
	}

	defer statement.Close()

	result, err := statement.Exec(book.Name, book.Author, book.Description, id)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return errors.New("cannot find a book to update")
	}

	return nil
}

func (o *Operator) Delete(id int) error {
	statementString := `DELETE FROM book WHERE id=?`

	statement, err := o.pool.Prepare(statementString)
	if err != nil {
		return err
	}

	defer statement.Close()

	_, err = statement.Exec(id)
	if err != nil {
		return err
	}

	return nil
}
