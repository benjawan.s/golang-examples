package db

import "database/sql"

// CreateTables is a migration method to create (if not exists) table in database. (just for this example)
func CreateTables(pool *sql.DB) error {
	statementString := `
	CREATE TABLE IF NOT EXISTS book
	(
	id INTEGER PRIMARY KEY,
	name VARCHAR(255),
	author VARCHAR(255),
	description TEXT,
	createdAt DATETIME NOT NULL DEFAULT (strftime('%Y-%m-%d %H:%M:%f', 'now', 'localtime')),
	updatedAt DATETIME NOT NULL DEFAULT (strftime('%Y-%m-%d %H:%M:%f', 'now', 'localtime'))
	)`

	statement, err := pool.Prepare(statementString)
	if err != nil {
		return err
	}

	defer statement.Close()

	_, err = statement.Exec()
	if err != nil {
		return err
	}

	return err
}
