package http

type JsonResponse struct {
	Code    int         `json:"code"`
	Payload interface{} `json:"payload,omitempty"`
	Message *string     `json:"message,omitempty"`
}
