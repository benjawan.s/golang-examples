package http

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
)

// JsonResponse is helper method to write out response back to http client.
func JsonResponse(ctx *fasthttp.RequestCtx, statusCode int, body interface{}) error {
	ctx.SetContentType("application/json; charset=utf-8")
	ctx.SetStatusCode(statusCode)

	responseBody, err := json.Marshal(body)
	if err != nil {
		return err
	}

	_, err = ctx.Write(responseBody)
	if err != nil {
		return err
	}

	return nil
}
