package context

import (
	"database/sql"
	"sync"
)

type Context struct {
	Pool *sql.DB
}

// one app Context per application
var ctx *Context = nil
var once sync.Once

func Ctx() *Context {
	once.Do(func() {
		ctx = &Context{}
	})
	return ctx
}
