module gitlab.com/nuttawutmalee/golang-examples

go 1.14

require (
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-sqlite3 v1.14.2
	github.com/prometheus/client_golang v1.7.1
	github.com/valyala/fasthttp v1.16.0
)
