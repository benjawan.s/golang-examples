package helper

import (
	"context"
	"errors"
	"fmt"
	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/app"
	context2 "gitlab.com/nuttawutmalee/golang-examples/internal/pkg/context"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/db"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/helper/project_path"
	httpModel "gitlab.com/nuttawutmalee/golang-examples/internal/pkg/model/http"
	dbInterface "gitlab.com/nuttawutmalee/golang-examples/pkg/interface/db"
	dbModel "gitlab.com/nuttawutmalee/golang-examples/pkg/model/db"
	"log"
	"net"
	"net/http"
	"os"
	"sort"
	"testing"
)

var (
	Host                        = "http://localhost"
	TestHTTPClient *http.Client = nil
	DBCluster      dbInterface.Pool
)

// SetupMainTest runs once before all tests
func SetupMainTest() (teardown func()) {
	// delete test database if exists
	_ = os.Remove(project_path.Root + "/tests/test_sqlite.db")

	// simulate env
	err := os.Setenv("SQLITE_FILE_LOCATION", ".\\/tests/test_sqlite.db")
	if err != nil {
		log.Fatal(err)
	}

	// serve in-memory http server
	l := fasthttputil.NewInmemoryListener()

	// mock database pool
	DBCluster = db.Cluster()

	// connect to database
	err = DBCluster.Connect()
	if err != nil {
		log.Fatal(err)
	}

	// define mock routes with mock app context
	router := fasthttprouter.New()
	app.DefineRoutes(router)

	go func() {
		_ = fasthttp.Serve(l, router.Handler)
	}()

	// create test http client that uses mock connection
	TestHTTPClient = &http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				conn, _ := l.Dial()
				return conn, nil
			},
		},
	}

	// teardown function runs after all tests are done
	return func() {
		// close http server
		_ = l.Close()

		// close database
		_ = DBCluster.Close()

		// unset env
		_ = os.Unsetenv("SQLITE_FILE_LOCATION")
	}
}

// SetupSubTest runs before each test
func SetupSubTest(t *testing.T) (teardown func(t *testing.T)) {
	// truncate tables before each test
	err := TruncateTable("book")
	if err != nil {
		t.Fatal(err)
	}

	// teardown function runs after each test is done
	return func(t *testing.T) {}
}

func ReconnectDatabase() error {
	_, err := DBCluster.Session()
	return err
}

func CompareJsonResponse(expected httpModel.JsonResponse, returned httpModel.JsonResponse) error {
	if expected.Code != returned.Code {
		return errors.New(fmt.Sprintf("expected code is %d, got %d", expected.Code, returned.Code))
	}

	if expected.Message != nil || returned.Message != nil {
		if expected.Message == nil && returned.Message != nil {
			return errors.New(fmt.Sprintf("expected message is nil, got %s", *returned.Message))
		}

		if returned.Message == nil && expected.Message != nil {
			return errors.New(fmt.Sprintf("expected message is %s, got nil", *expected.Message))
		}

		if *expected.Message != *returned.Message {
			return errors.New(fmt.Sprintf("expected message is %s, got %s", *expected.Message, *returned.Message))
		}
	}

	return nil
}

func CompareBook(expected dbModel.Book, returned dbModel.Book) error {
	if expected.ID != returned.ID {
		return errors.New(fmt.Sprintf("expected id is %d, got %d", expected.ID, returned.ID))
	}

	if expected.Name != returned.Name {
		return errors.New(fmt.Sprintf("expected name is %s, got %s", expected.Name, returned.Name))
	}

	if expected.Author != returned.Author {
		return errors.New(fmt.Sprintf("expected author is %s, got %s", expected.Author, returned.Author))
	}

	if expected.Description != returned.Description {
		return errors.New(fmt.Sprintf("expected description is %s, got %s", expected.Description, returned.Description))
	}

	return nil
}

func CompareBooks(expected []dbModel.Book, returned []dbModel.Book) error {
	if len(expected) != len(returned) {
		return errors.New(fmt.Sprintf("expected results length of %d, got length of %d", len(expected), len(returned)))
	}

	sort.Slice(expected, func(i int, j int) bool {
		return expected[i].ID <= expected[j].ID
	})

	sort.Slice(returned, func(i int, j int) bool {
		return returned[i].ID <= returned[j].ID
	})

	for i, returnedBook := range returned {
		expectedBook := expected[i]

		err := CompareBook(expectedBook, returnedBook)
		if err != nil {
			return err
		}
	}

	return nil
}

func GetOneBook(id int) (book *dbModel.Book, err error) {
	queryString := `SELECT * FROM book WHERE id = ?`

	rows, err := context2.Ctx().Pool.Query(queryString, id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var emptyBook dbModel.Book

		err := rows.Scan(
			&emptyBook.ID,
			&emptyBook.Name,
			&emptyBook.Author,
			&emptyBook.Description,
			&emptyBook.CreateAt,
			&emptyBook.UpdateAt)

		if err != nil {
			return nil, err
		}

		book = &emptyBook
		break
	}

	if book == nil {
		return nil, errors.New("book not found")
	}

	return book, nil
}

func GetAllBooks() (books []dbModel.Book, err error) {
	queryString := `SELECT id, name, author, description, createdAt, updatedAt FROM book`

	rows, err := context2.Ctx().Pool.Query(queryString)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var emptyBook dbModel.Book

		err := rows.Scan(
			&emptyBook.ID,
			&emptyBook.Name,
			&emptyBook.Author,
			&emptyBook.Description,
			&emptyBook.CreateAt,
			&emptyBook.UpdateAt)

		if err != nil {
			return nil, err
		}

		books = append(books, emptyBook)
	}

	return books, nil
}

func CreateBook(book dbModel.Book) (id int64, err error) {
	statementString := `INSERT INTO book (name, author, description) VALUES (?, ?, ?)`

	statement, err := context2.Ctx().Pool.Prepare(statementString)
	if err != nil {
		return id, err
	}

	defer statement.Close()

	result, err := statement.Exec(book.Name, book.Author, book.Description)
	if err != nil {
		return id, err
	}

	id, err = result.LastInsertId()
	if err != nil {
		return id, err
	}

	return id, nil
}

func TruncateTable(tableName string) error {
	statementString := fmt.Sprintf("DELETE FROM %s", tableName)

	statement, err := context2.Ctx().Pool.Prepare(statementString)
	if err != nil {
		return err
	}

	defer statement.Close()

	_, err = statement.Exec()
	if err != nil {
		return err
	}

	return nil
}
