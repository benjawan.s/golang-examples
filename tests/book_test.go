package tests

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/valyala/fasthttp"
	http2 "gitlab.com/nuttawutmalee/golang-examples/internal/pkg/model/http"
	"gitlab.com/nuttawutmalee/golang-examples/pkg/model/db"
	"gitlab.com/nuttawutmalee/golang-examples/tests/helper"
	"io/ioutil"
	"net/http"
	"strconv"
	"testing"
)

/**
Create a book (API)
*/

func TestCreateBookSuccess(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	requestBody := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	bodyBytes, err := json.Marshal(requestBody)
	if err != nil {
		t.Fatal(err)
	}

	// Define expectation
	expectedResponse := http2.JsonResponse{
		Code:    201,
		Payload: nil,
	}
	expectedCreatedBook := db.Book{
		ID:          1,
		Name:        requestBody.Name,
		Author:      requestBody.Author,
		Description: requestBody.Description,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodPost, helper.Host+"/books", bytes.NewBuffer(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	dbBook, err := helper.GetOneBook(1)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(expectedCreatedBook, *dbBook)
	if err != nil {
		t.Fatal(err)
	}
}

func TestCreateBookFailedInvalidRequestBody(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// Define expectation
	errorMessage := "cannot unmarshal request body : invalid character 'I' looking for beginning of value"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodPost, helper.Host+"/books", bytes.NewBuffer([]byte("INVALID")))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	allBooks, err := helper.GetAllBooks()
	if err != nil {
		t.Fatal(err)
	}

	if len(allBooks) > 0 {
		t.Fatal(errors.New("book table must be empty"))
	}
}

func TestCreateBookFailedCannotCreateRecordInDatabase(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// disconnect database
	err := helper.DBCluster.Close()
	if err != nil {
		t.Fatal(err)
	}

	requestBody := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	bodyBytes, err := json.Marshal(requestBody)
	if err != nil {
		t.Fatal(err)
	}

	// Define expectation
	errorMessage := "cannot create a book : sql: database is closed"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodPost, helper.Host+"/books", bytes.NewBuffer(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// reconnect database
	err = helper.ReconnectDatabase()
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	allBooks, err := helper.GetAllBooks()
	if err != nil {
		t.Fatal(err)
	}

	if len(allBooks) > 0 {
		t.Fatal(errors.New("book table must be empty"))
	}
}

/**
Get all books (API)
*/

func TestGetAllBookSuccess(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}
	secondBook := db.Book{
		Name:        "Cathedral 2",
		Author:      "Raymond Carter",
		Description: "XYZ...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	secondBookId, err := helper.CreateBook(secondBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)
	secondBook.ID = int(secondBookId)

	// Define expectation
	expectedBooks := []db.Book{
		firstBook,
		secondBook,
	}
	expectedResponse := http2.JsonResponse{
		Code:    200,
		Payload: expectedBooks,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodGet, helper.Host+"/books", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	books, err := helper.GetAllBooks()
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBooks(expectedBooks, books)
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetAllBookFailedCannotGetRecordFromDatabase(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}
	secondBook := db.Book{
		Name:        "Cathedral 2",
		Author:      "Raymond Carter",
		Description: "XYZ...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	secondBookId, err := helper.CreateBook(secondBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)
	secondBook.ID = int(secondBookId)

	// disconnect database
	err = helper.DBCluster.Close()
	if err != nil {
		t.Fatal(err)
	}

	// Define expectation
	expectedBooks := []db.Book{
		firstBook,
		secondBook,
	}

	errorMessage := "cannot get books : sql: database is closed"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
		Payload: make([]db.Book, 0),
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodGet, helper.Host+"/books", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// reconnect database
	err = helper.ReconnectDatabase()
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	allBooks, err := helper.GetAllBooks()
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBooks(expectedBooks, allBooks)
	if err != nil {
		t.Fatal(err)
	}
}

/**
Get a book (API)
*/

func TestGetOneBookSuccess(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	expectedResponse := http2.JsonResponse{
		Code:    200,
		Payload: firstBook,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodGet, helper.Host+"/books/"+strconv.Itoa(firstBook.ID), nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetOneBookFailedCannotGetRecordFromDatabase(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	errorMessage := "cannot get a book : book not found"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodGet, helper.Host+"/books/999", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetOneBookFailedCannotParseIDToInteger(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	errorMessage := "cannot convert id to integer : strconv.Atoi: parsing \"unknown\": invalid syntax"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodGet, helper.Host+"/books/unknown", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}
}

/**
Update a book (API)
*/

func TestUpdateBookSuccess(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	updatedBook := db.Book{
		ID:          firstBook.ID,
		Name:        "Cathedral 2",
		Author:      "Raymond Carter",
		Description: "XYZ...",
	}

	expectedResponse := http2.JsonResponse{
		Code:    200,
		Payload: updatedBook,
	}

	bodyBytes, err := json.Marshal(updatedBook)
	if err != nil {
		t.Fatal(err)
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodPut, helper.Host+"/books/"+strconv.Itoa(firstBook.ID), bytes.NewBuffer(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(updatedBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err == nil {
		t.Fatal(errors.New("book must be updated"))
	}
}

func TestUpdateBookFailedCannotUpdateRecordInDatabase(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	updatedBook := db.Book{
		ID:          firstBook.ID,
		Name:        "Cathedral 2",
		Author:      "Raymond Carter",
		Description: "XYZ...",
	}

	errorMessage := "cannot update a book : cannot find a book to update"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
	}

	bodyBytes, err := json.Marshal(updatedBook)
	if err != nil {
		t.Fatal(err)
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodPut, helper.Host+"/books/2", bytes.NewBuffer(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(updatedBook, *oneBook)
	if err == nil {
		t.Fatal(errors.New("book must not be updated"))
	}
}

func TestUpdateBookFailedInvalidRequestBody(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	updatedBook := db.Book{
		ID:          firstBook.ID,
		Name:        "Cathedral 2",
		Author:      "Raymond Carter",
		Description: "XYZ...",
	}

	errorMessage := "cannot unmarshal request body : invalid character 'I' looking for beginning of value"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodPut, helper.Host+"/books/"+strconv.Itoa(firstBook.ID), bytes.NewBuffer([]byte("INVALID")))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(updatedBook, *oneBook)
	if err == nil {
		t.Fatal(errors.New("book must not be updated"))
	}
}

func TestUpdateBookFailedCannotGetRecordFromDatabase(t *testing.T) {

}

func TestUpdateBookFailedCannotParseIDToInteger(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	updatedBook := db.Book{
		ID:          firstBook.ID,
		Name:        "Cathedral 2",
		Author:      "Raymond Carter",
		Description: "XYZ...",
	}

	errorMessage := "cannot convert id to integer : strconv.Atoi: parsing \"unknown\": invalid syntax"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
	}

	bodyBytes, err := json.Marshal(updatedBook)
	if err != nil {
		t.Fatal(err)
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodPut, helper.Host+"/books/unknown", bytes.NewBuffer(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(updatedBook, *oneBook)
	if err == nil {
		t.Fatal(errors.New("book must not be updated"))
	}
}

/**
Delete a book (API)
*/

func TestDeleteBookSuccess(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	expectedResponse := http2.JsonResponse{
		Code:    204,
		Message: nil,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodDelete, helper.Host+"/books/"+strconv.Itoa(firstBook.ID), nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	_, err = helper.GetOneBook(firstBook.ID)
	if err == nil {
		t.Fatal(errors.New("book must be deleted"))
	}
}

func TestDeleteBookReturnSuccessEvenBookIsNotFound(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	expectedResponse := http2.JsonResponse{
		Code: 204,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodDelete, helper.Host+"/books/9999", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}
}

func TestDeleteBookFailedCannotDeleteRecordInDatabase(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// disconnect database
	err = helper.DBCluster.Close()
	if err != nil {
		t.Fatal(err)
	}

	// Define expectation
	errorMessage := "cannot delete a book : sql: database is closed"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodDelete, helper.Host+"/books/"+strconv.Itoa(firstBook.ID), nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// reconnect database
	err = helper.ReconnectDatabase()
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}
}

func TestDeleteBookFailedCannotParseIDToInteger(t *testing.T) {
	// Prepare
	teardown := helper.SetupSubTest(t)
	defer teardown(t)

	// create books in db
	firstBook := db.Book{
		Name:        "Cathedral",
		Author:      "Raymond Carver",
		Description: "ABC...",
	}

	firstBookId, err := helper.CreateBook(firstBook)
	if err != nil {
		t.Fatal(err)
	}

	firstBook.ID = int(firstBookId)

	// Define expectation
	errorMessage := "cannot convert id to integer : strconv.Atoi: parsing \"unknown\": invalid syntax"
	expectedResponse := http2.JsonResponse{
		Code:    500,
		Message: &errorMessage,
	}

	// When
	req, err := http.NewRequest(fasthttp.MethodDelete, helper.Host+"/books/unknown", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")

	res, err := helper.TestHTTPClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// Then
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	defer res.Body.Close()

	var returnedResponse http2.JsonResponse
	err = json.Unmarshal(data, &returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareJsonResponse(expectedResponse, returnedResponse)
	if err != nil {
		t.Fatal(err)
	}

	oneBook, err := helper.GetOneBook(firstBook.ID)
	if err != nil {
		t.Fatal(err)
	}

	err = helper.CompareBook(firstBook, *oneBook)
	if err != nil {
		t.Fatal(err)
	}
}
