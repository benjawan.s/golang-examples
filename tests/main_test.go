package tests

import (
	"gitlab.com/nuttawutmalee/golang-examples/tests/helper"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	// bootstrap tests
	teardown := helper.SetupMainTest()

	// wait til all cases are down then run teardown method
	// to clean all unnecessary data
	defer teardown()

	code := m.Run()
	os.Exit(code)
}
