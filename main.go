package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/nuttawutmalee/golang-examples/internal/pkg/app"
	"log"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	if err := app.New().Start(); err != nil {
		log.Fatal(err)
	}
}
