package app

// Bootstrapper is an interface to define how to start an application.
type Bootstrapper interface {
	Start() error
}
