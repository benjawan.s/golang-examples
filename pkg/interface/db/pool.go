package db

// Pool is an interface to define how to start, use, and close the database.
type Pool interface {
	Connect() error
	Session() (session interface{}, err error)
	Close() error
}
