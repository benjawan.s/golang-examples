package book

import (
	"gitlab.com/nuttawutmalee/golang-examples/pkg/model/db"
)

// Operator is an interface to define how we can interact with object in the database.
type Operator interface {
	Create(book db.Book) error
	GetAll() (books []db.Book, err error)
	GetOne(id int) (book *db.Book, err error)
	Update(id int, book db.Book) error
	Delete(id int) error
}
